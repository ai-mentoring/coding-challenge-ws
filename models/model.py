from abc import abstractmethod, ABC

from .storage_handler import StorageHandler


class Model(ABC):

    __slots__ = ["_version",
                 "_handler"]

    def __init__(self, handler: StorageHandler):
        self._version = "0.6.19"
        self._handler = handler

    @abstractmethod
    def talk(self, user_input: str) -> str:
        pass

    @abstractmethod
    def get_content(self):
        pass

    def dump(self):
        self._handler.store(self)
