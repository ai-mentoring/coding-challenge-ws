from os.path import expanduser

from models.example_model import ExampleModel
from models.model import Model
from models.storage_handler import StorageHandler


def start(model: Model):

    user_input = ""

    while user_input.lower() != "bye":

        user_input = input("Max:")
        response = model.talk(user_input)
        print("Donald:{}".format(response))

    model.dump()


if __name__ == "__main__":

    sh = StorageHandler(expanduser("~"))
    # TODO: change the model to your implementation
    m = ExampleModel(sh)
    start(m)
